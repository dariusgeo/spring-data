package sda.spring.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring/application-config.xml"})
public class EmployeeServiceIT {
	
	@Autowired
	private EmployeeService employeeService;
	
	@Test
	public void testFindAll() {
		employeeService.findAll().forEach(emp -> {
			System.out.println(String.format("Name == %d %s %s", emp.getEmpNo(), emp.getFirstName(), emp.getLastName()));
		});
	}
	
	@Test
	public void testFindAllByLastname() {
		employeeService.findAllByLastName("Waterhouse").forEach(emp -> {
			System.out.println(String.format("Name == %d %s %s", emp.getEmpNo(), emp.getFirstName(), emp.getLastName()));
		});	
	}
	
	@Test
	public void testFindAllByDepartmentName() {
		employeeService.findAllByDeptName("Marketing").forEach(emp -> {
			System.out.println(String.format("Name == %d %s %s", emp.getEmpNo(), emp.getFirstName(), emp.getLastName()));
		});	
	}

}
