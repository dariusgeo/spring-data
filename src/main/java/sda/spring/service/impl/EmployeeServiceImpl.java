package sda.spring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sda.spring.dao.EmployeeRepository;
import sda.spring.model.Employee;
import sda.spring.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	
	@Autowired
	private EmployeeRepository repository;

	@Override
	public List<Employee> findAll() {

		return (List<Employee>) repository.findAll();
	}

	@Override
	public List<Employee> findAllByLastName(String lastname) {
		
		return repository.findAllByLastName(lastname);
	}

	@Override
	public List<Employee> findAllByDeptName(String deptName) {

		return repository.findByDepartments_DeptName(deptName);
	}

}
