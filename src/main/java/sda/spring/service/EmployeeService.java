package sda.spring.service;

import java.util.List;

import sda.spring.model.Employee;

public interface EmployeeService {

	List<Employee> findAll();
	
	List<Employee> findAllByLastName(String lastname);
	
	List<Employee> findAllByDeptName(String deptName);
}
